﻿define(['sammy'], function (Sammy) {
    
    
    var routeDecoupler = function() {
        var self = this;
        self.sammy = new Sammy();
        self.maps = {};

        var _makeArray = function (nonarray) { return Array.prototype.slice.call(nonarray); }
        var STRIP_COMMENTS = /((\/\/.*$)|(\/\*[\s\S]*?\*\/))/mg;
        function getParamNames(func) {
            var fnStr = func.toString().replace(STRIP_COMMENTS, '');
            var result = fnStr.slice(fnStr.indexOf('(') + 1, fnStr.indexOf(')')).match(/([^\s,]+)/g);
            if (result === null)
                result = [];
            return result;
        }

        //adds route mapping for view model
        self.addRouteMapping = function (model, mapping, mappingKey) {
            if (!model || !mapping || !mappingKey)
                return;

            var reload = self.sammy.isRunning();

            if (reload)
                unloadRouting();

            if (self.maps[mappingKey])
                self.removeRouteMapping(mappingKey);

            self.maps[mappingKey] = { model: model, maps: mapping };

            if (reload)
                self.applyRoutes();

            return self;
        }
        //removes route mapping by given key
        self.removeRouteMapping = function (mappingKey) {
            if (!mappingKey)
                return;

            var model = self.maps[mappingKey].model;
            var maps = self.maps[mappingKey].maps;

            if (!model || !maps)
                return;

            maps.forEach(function (item) {
                if (!model[item.on]._sammyCallback)
                    return;
                model[item.on] = model[item.on]._sammyCallback;
                model[item.on]._sammyCallback = undefined;
            });

            delete self.maps[mappingKey];

            if (self.sammy.isRunning()) {
                unloadRouting();
                self.applyRoutes();
            }

            return self;
        }
        //applies all route mappings
        self.applyRoutes = function (takeLastDefault) {

            if (self.sammy.isRunning())
                return;

            var defRoute = null;

            for (var key in self.maps)
            {
                var model = self.maps[key].model;
                var maps = self.maps[key].maps;

                var maps2 = maps.map(function (item) {

                    if (!model[item.on])
                    {
                        console.log("browserRouting: method " + item.on + " doesn't exist");
                        return undefined;
                    }

                    if (model[item.on]._sammyCallback) {
                        model[item.on] = model[item.on]._sammyCallback;
                        model[item.on]._sammyCallback = undefined;
                    }
                    
                    var callback = model[item.on];
                    var path = item.path;
                    var pathBuilder = item.pathBuilder ? item.pathBuilder : function () { return path };

                   

                    model[item.on] = function () {
                        location.hash = pathBuilder.apply(undefined, _makeArray(arguments));
                    };
                    model[item.on]._sammyCallback = callback;

                    if (item.default && (takeLastDefault || !defRoute))
                        defRoute = '#' + path;
                    return { path: '#' + path, callback: callback, params: getParamNames(callback) };
                });

                maps2.forEach(function (item) {
                    if (!item)
                        return;

                    var params = item.params;
                    var callback = item.callback;
                    self.sammy.get(item.path, function (context) {
                        var args = [];
                        params.forEach(function (p) {
                            args.push(context.params[p]);
                        });

                        callback.apply(model, args);
                    });
                });
            }

            if (defRoute)
                self.sammy.get('', function () { self.sammy.runRoute('get', defRoute) });
            else
                self.sammy.get('', function () {});

            self.sammy.run();
            return self;
        };

        var unloadRouting = function () {
            self.sammy.unload();
            delete self.sammy.routes.get;
            return self;
        };

        self.removeAllRouteMappings = function () {

            unloadRouting();
            
            for (var key in self.maps) {
                self.removeRouteMapping(key);
            }

            location.hash = undefined;

            return self;
        };

    }

    return new routeDecoupler();
});