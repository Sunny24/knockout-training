﻿var App = App || {};

(function ($, ko) {
    "use strict";
    App.FlickrSearch = function () {
        var self = this;
        self.url = 'http://api.flickr.com/services/feeds/photos_public.gne?jsoncallback=?';
        self.images = ko.observableArray();
        self.searchTerm = ko.observable("");
        
        self.search = function () {
            
            if (self.searchTerm().length > 0) {
                self.images([]);
                $.getJSON(self.url, {
                    tags: self.searchTerm(),
                    tagmode: "any",
                    format: "json"
                }).done(function (data) {
                    $.each(data.items, function (i, item) {
                        var model = new App.FlickrSearch.ImageModel(item.title, item.media.m, Math.floor((Math.random() * 6) + 1));
                        self.images.push(model);
                    });
                });
            }
        };
    };

    App.FlickrSearch = App.FlickrSearch || {};
    App.FlickrSearch.ImageModel = function(title, thumbUrl, rating) {
        this.title = title;
        this.thumbUrl = thumbUrl;
        this.userRating = ko.observable(rating || null);
    };
}(jQuery, ko));