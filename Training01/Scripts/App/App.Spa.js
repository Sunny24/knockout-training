﻿var App = App || {};

(function ($, ko) {
    "use strict";
    App.SinglePageApplication = function () {
        var self = this;
        self.views = ko.observableArray(["Home", "Images", "About", "Contact"]);
        self.currentView = ko.observable();

        self.imagesHandler = new App.ImagesHandler();

        self.goToFolder = function(folder) {
             location.hash = folder;
        };

        self.goToDetails = function(image) {
             location.hash = image.folder + '/' + image.id;
        };


        // Client-side routes    
        Sammy(function () {
            this.get('#:view', function () {
                //Set currentView on your view model
                self.currentView(this.params.view);
            });

            //this.get('#:views/:imageId', function () {
            //    self.chosenFolderId(this.params.folder);
            //    self.chosenFolderData(null);
            //    // $.get("/images", { mailId: this.params.mailId }, self.chosenMailData);
            //});

            this.get('', function () { this.app.runRoute('get', '#Home'); });
        }).run();

    };

    App.ImagesHandler = function() {
        var self = this;

        self.images = ko.observableArray();

        self.init = function() {
            for (var i = 0; i < 2; i++) {
                var img = new App.SinglePageApplication.ImageModel("Image " + i, "http://placehold.it/200/337ab7/ffffff&text=IMG_" + i, 2, i);
                self.images.push(img);
            }
        };

        self.init();
    };

    App.SinglePageApplication = App.SinglePageApplication || {};
    App.SinglePageApplication.ImageModel = function (title, thumbUrl, rating, id) {
        this.title = title;
        this.thumbUrl = thumbUrl;
        this.userRating = ko.observable(rating || null);
        this.id = id;
    };

}(jQuery, ko));