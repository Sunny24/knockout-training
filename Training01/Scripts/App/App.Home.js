﻿/// <reference path="../knockout-3.2.0.js" />


var App = App || {};
(function ($, ko) {
	"use strict";
	App.Home = function () {
		var self = this; // scope 
		self.firstName = ko.observable('Peter');
		self.lastName = ko.observable('Pan');

		self.fullName = ko.pureComputed(function () {
			return self.firstName() + ", " + self.lastName();
		});

		self.selectedCulture = ko.observable();
	    self.selectedCultures = ko.observableArray();
		self.cultures = ko.observableArray([
			new App.Home.CultureModel("German", "de"),
			new App.Home.CultureModel("usa", "en"),
			new App.Home.CultureModel("France", "fr"),
			new App.Home.CultureModel("Italia", "it")
		]);

		self.deletedCultures = ko.observableArray();

		self.deleteCulture = function () {
			self.deletedCultures.push(this);
	        self.cultures.remove(this);
		};

	    self.checkboxValue = ko.observable(false);

	    self.scriptCode = ko.observable("<h1>Script code</h1>");

	    self.detailsEnabled = ko.observable(false),
	    self.enableDetails = function() {
	        this.detailsEnabled(true);
	    };

	    self.disableDetails = function () {
	        this.detailsEnabled(false);
	    };

	    self.person = new App.Home.PersonModel();
	};

	App.Home = App.Home || {};
	App.Home.CultureModel = function (name, code) {
		this.name = name;
	    this.code = code;
	};

	App.Home.PersonModel = function () {
	    this.firstName = 'First Name';
	    this.lastName = 'Last Name';
	    this.city = 'City';
	    this.country = 'Country';
	};


}(jQuery, ko)); //IIFE
