﻿
ko.components.register('star-rating', {
    viewModel: function (params) {
        var self = this;
        // Data: value is either null, 'like', or 'dislike'
        self.points = params.value;
        self.starsCount = params.starsCount;
        self.starsArray = ko.observableArray();

        var rateItem = function(index, isActive) {
            var me = this;
            me.isActive = ko.observable(isActive);
            me.index = index;
            me.cssClass = ko.computed(function () {
                var css = me.isActive() ? "chosen" : "";
                return css;
            });
        }

        self.setRating = function (rate) {
            self.points(rate.index);
            for (var j = 0; j < self.starsCount; j++) {
                var activated = j < self.points();
                self.starsArray()[j].isActive(activated);
            }
        };

        for (var i = 0; i < self.starsCount; i++) {
            var active = i  < self.points();
            self.starsArray.push(new rateItem(i + 1, active));
        }
    },
    template:
        '<div class="starRating" data-bind="foreach : starsArray">\
            <span data-bind="click: $parent.setRating, css: cssClass "></span>\
        </div>'
});

ko.bindingHandlers.href = {
    update: function (element, valueAccessor) {
        ko.bindingHandlers.attr.update(element, function () {
            return { href: valueAccessor() }
        });
    }
};

ko.bindingHandlers.src = {
    update: function (element, valueAccessor) {
        ko.bindingHandlers.attr.update(element, function () {
            return { src: valueAccessor() }
        });
    }
};

ko.bindingHandlers.toJSON = {
    update: function (element, valueAccessor) {
        return ko.bindingHandlers.text.update(element, function () {
            return ko.toJSON(valueAccessor(), null, 2);
        });
    }
};